﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using pdf_replacer.PdfReplace;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pdf_replacer.Controllers
{
    public class PdfFrontController : Controller
    {
        private readonly IHostingEnvironment _env;
        private string uploadPath = "";
        private string uploadDirName = "uploads";
        private string serverBaseUrl = "http://localhost:7848/";
        public PdfFrontController(IHostingEnvironment IHostin)
        {
            this._env =  IHostin;
            this.uploadPath = Path.Combine(this._env.WebRootPath,this.uploadDirName)+"/";

        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("UploadFile")]
        //name="file" and IFormForm file, variable name and input name should be 
        public IActionResult Upload(IFormFile file,string search="[name]",string replace = "_REPLACED_")
        {
            string uploadedFileName = "";

            ViewBag.fileName = "nope";
            if(file!=null) 
            {

                uploadedFileName = file.FileName;
                ViewBag.fileName = uploadedFileName;


                NameHelper uploadedFileNameHelper = new NameHelper(uploadedFileName,this.uploadPath,this.uploadDirName,this.serverBaseUrl);

                NameHelper replacedFileNameHelper = new NameHelper(uploadedFileName, this.uploadPath, this.uploadDirName, this.serverBaseUrl);


                //upload => fs => pdf replacer base => pdf replacer target
                
                using(FileStream fs = System.IO.File.Create(uploadedFileNameHelper.getNewSavePath()))
                {

                    file.CopyTo(fs);
                    fs.Close();
                    PdfReplacer pdfReplacer = new PdfReplacer(uploadedFileNameHelper.getNewSavePath(),replacedFileNameHelper.getNewSavePath());
                    pdfReplacer.replace(search,replace);
                    pdfReplacer.save();



                    JsonResponse jsonResponse = new JsonResponse();

                    jsonResponse.replacedFilePath = replacedFileNameHelper.getDownloadUrl();

                    return Json(jsonResponse);
                    //fs.Flush();

                }
                
            }
            return View();
        }
    }
}
