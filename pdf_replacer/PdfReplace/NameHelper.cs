﻿using System;
using System.IO;

namespace pdf_replacer.PdfReplace
{
    public class NameHelper
    {
      
        public string _fileName = "";
        public string _fileExtension = "";
        public string _newFileName = "";
        public string _baseUrl = "";
        private string _uploadDir = "";
        private string _uploadPath = "";
        public NameHelper(string fileName = "",string uploadPath = "",string uploadDir =  "",string baseUrl = "")
        {
            this._fileName = fileName;
            this._fileExtension = Path.GetExtension(fileName);
            this._newFileName = Convert.ToString(Guid.NewGuid())+_fileExtension;
            this._baseUrl = baseUrl;
            this._uploadDir = uploadDir;
            this._uploadPath = uploadPath;

        }


        public string getDownloadUrl()
        {
            return this._baseUrl + this._uploadDir + "/" + this._newFileName;
        }

        public string getSavePath()
        {
            return this._uploadPath  + this._fileName;
        }

        public string getNewSavePath()
        {
            return this._uploadPath  + this._newFileName;
        }


    }
}

