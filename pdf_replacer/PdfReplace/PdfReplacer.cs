﻿using System;
using System.Text;
namespace pdf_replacer.PdfReplace
{
    public class PdfReplacer
    {

            

        private string _sourceFilePath;
        private string _targetFilePath;
        private PdfSharp.Pdf.PdfDocument _exportDocument = new PdfSharp.Pdf.PdfDocument();
        public PdfReplacer(string sourceFilePath, string targetFilePath)
        {
            _sourceFilePath = sourceFilePath;
            _targetFilePath = targetFilePath;

        }


        public  void replace(string searching, string replacing)
        {

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding myEncoding = Encoding.GetEncoding("ISO-8859-9");


            string loadPath = this._sourceFilePath;
            string destPath = this._targetFilePath;

            byte[] inStream;
            string replaceString = replacing;
            byte[] replaceBytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(replaceString);
            string replaceStringFromBytes = Encoding.GetEncoding("ISO-8859-9").GetString(replaceBytes);

            byte[] outStream;
            string stringStream;

            PdfSharp.Pdf.PdfDocument importDoc = PdfSharp.Pdf.IO.PdfReader.Open(loadPath, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
            for (int i = 0; i < importDoc.PageCount; i++)
            {
                var newPage = importDoc.Pages[i];
                stringStream = "";

                for (int j = 0; j < newPage.Contents.Elements.Count; j++)
                {
                    PdfSharp.Pdf.PdfDictionary.PdfStream stream = newPage.Contents.Elements.GetDictionary(j).Stream;
                    inStream = stream.Value;

                    foreach (byte b in inStream)
                        stringStream += (char)b;

                    Console.WriteLine("stream l is => {0}", inStream.Length);


                    stringStream = stringStream.Replace(searching, replaceString);

                    outStream = Encoding.GetEncoding("ISO-8859-9").GetBytes(stringStream);
                    newPage.Contents.Elements.GetDictionary(j).Stream.Value = outStream;
                }
                var myPage = new PdfSharp.Pdf.PdfPage();
                myPage = newPage;
                newPage = this._exportDocument.AddPage(myPage);
            }

        }

        public void save()
        {
            this._exportDocument.Save(this._targetFilePath);
        }

    }
}

